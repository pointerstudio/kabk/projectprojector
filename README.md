 > # Disclaimer
> In some cases it would make sense to make an appointment with the 3D lab (see below). Don't forget, that many ideas could be realised low-tech without even the need of Unity or Blender!

 ```
 p r o j e c t           "                   m                 
 mmmm    m mm   mmm    mmm    mmm    mmm   mm#mm   mmm    m mm 
 #" "#   #"  " #" "#     #   #"  #  #"  "    #    #" "#   #"  "
 #   #   #     #   #     #   #""""  #        #    #   #   #    
 ##m#"   #     "#m#"     #   "#mm"  "#mm"    "mm  "#m#"   #    
 #                       #                                     
 "                     ""                                      

Course: GOVT2 Interactive Media Design 2021
Assignment 2: Project Projector
```

# Description
A projector can be used to show movies in your home. To make this possible, it throws an image at a regular wall and thereby turns it into a screen. The projector cannot only turn walls into screens, it can turn anything into a screen. If you walk between the projector and the wall, the parts of you that face the projector are becoming a screen, and the image is wrapped around the shape of your body. And you can go further with this. If you put a milk carton in between the projector and the wall, and then you project an image of a skyscraper onto its surface, then the milk carton transforms into a part of an urban landscape.

The projector can be used as a glue between digital and analogue.

For this assignment you will have to create spatial installations using a simple technique for projection mapping. Using blender or unity and following the tutorial which we will prepare, you will have to create visuals which will be projected onto one or multiple physical objects in space. Having the technique as a starting point, you are free to interpret the assignment in any way that fits your idea - it can be small or big, moving or static, abstract or based on a specific narrative. You can use the projection mapping technique, or use the projector in a different, possibly simpler, way. Anything goes, as long as you explain your choices.

`T H I S  A S S I G N M E N T  -I -S  =F =A =S =T`

Keep in mind the short duration of this task. Don’t overcomplicate, go with your gut feeling and no need to overconceptualize if it doesn’t come to you naturally. Make sure you start working immediately and already use the first week as much as you can.

You will work in groups of three or maximum four students. Try to discuss and assign responsibilities early on in order to make use of each others' strengths and make the process less stressful on every level. You can do it.

Think about: 
- Which object/s are you going to project your visuals on?
- What can your projection be and how will it add a new layer of meaning to this object
  (even if your visuals are abstract)
- How can you make this exciting for you personally, even though you’re working in groups.
  Maybe you’re interested in creating an analogue object to project on, or in exploring a
  tool like blender, or making a performative work.

Sidenote: Don’t feel intimidated when searching for projection mapping online. This can be a pretty advanced technique, but it can also be simply pointing a projector to the ceiling and hold baloons on sticks into the thrown image. 

# Expected outcome:
A physical installation involving a projection.

# Useful links:

0. Assignment repository:
	  https://gitlab.com/pointerstudio/kabk/projectprojector

1. Inspirations:
    https://www.are.na/share/NNcDzne
	  (by the way, we saw many of you use are.na already, and we can definitely recommend it as a research gathering tool, also - and especially - for collective work)
# 3D lab:
https://portal.kabk.nl/the-kabk/student-services/workshops/3d-lab/about-contact-1

The KABK has a 3D lab with tons of equipment and very nice people. They are experts on 3D printing and scanning. Should you want to make a scan of an object to project on, they are happy to assist you. Make sure, that you email them first to arrange an appointment since they are superbusy now before the christmas break. We warned them, that you might be knocking on their door.

Keep in mind, this is for rather advanced projections, where the shape of the object you project on is rather intricate and important. You probably don’t need this, but if you really want: do it.

# Projection Mapping Tutorial

 ## 1. Create a 3D model of your object - you can do it in various programs. The way we did this is making pictures of the object with a camera and merged them into a photogrammetry model with an external program, in our case Meshroom, but unfortunately it's not compatible with Mac. there are many alternatives though, like regard3D and others. But you can also do it with a phone app, please research your options. The only important thing is that the format of the model should be in the end **compatible with Blender (OBJ, FBX, 3DS, PLY, STL, etc.)**.

 ## 2. Open your model in blender, clean up your model and rotate it to the right position:

![Sample Video](1-tutorial-clean-and-move.mp4)

### **2.1.** delete the default cube:
- **select** the cube > **right click** -> **delete**

### **2.2.** import your model: 
- **file** -> **import** -> **select the format of your model** -> **select the file** -> wait a few seconds until it is loaded 

### **2.3** clean your model:
- Select your model and go to **Edit Mode** (top left of the screen). In **Edit Mode** you will be able to adjust the **mesh** of your model - here we will clean the unnecessary parts for instance.

### **2.4** rotate and position:
- We also want to rotate and position it properly, for that it is very handy to use **numpad (1,2,3,etc.)** keys or the **XYZ tool** at the top right corner - you will switch between the different perspectives and will be able to straighten your model next to a grid. 

![Image](Screenshot_2021-12-19_at_17.42.58.png)

### **2.5** select and delete the unwanted parts of your model:
- Go to **Edit Mode** > select the **Wireframe mode** at the top right corner of your viewport:

￼![Image](Screenshot_2021-12-19_at_17.35.52.png)

- this will make it easier to select the mesh, otherwise it will only select the parts which are visible (so not at the back of the model)
- now just use **Select tool** to select and **right click** -> **Delete Vertices** to delete 


 ## 3. Create new simplified UV map:

![Sample Video](2-tutorial-simplification-of-mesh-and-uv-mapping.mp4)

### **3.1** simplify the mesh (optional, but can be good to make blender work faster and have smoother projection): 

- go to **Modifier Properties** > **Add Modifier** > **Remesh** > **Smooth** > change **Octree Depth** number to change the level of detail of your model's mesh > **Apply (important)** 
- now: **Modifier Properties** > **Add Modifier** > **Decimate** > change **Ratio Number** to simplify your mesh to the needed level > **Apply**
	
### **3.2** UV mapping: 

- **UV mapping** is defining how the texture is going to be wrapped around the model. You do not need to waste texture on invisible parts, for instance the bottom, so let’s delete it
- go to **Edit Mode** > select **Solid Shading view** (top right same as the **Wireframe View**) > select and delete the “floor” of your model - or whichever part is not relevant for your projection.
- now go to **UV Editing mode** > in the left half of the window add new image by clicking to **New** > select **Color Grid** as **Generated Type** > **OK**
- on the right side (where the model is) click **select** > **all**, then click **UV** > **unwrap** and wait until it’s finished - might take a bit depending on how big your model is
- when it’s done, you will see your mesh distributed across the texture on the left. Let’s save the UV image which we created so that we can apply it as texture for our model. Do **image** > **save**

### **3.3.** Apply image texture to your model:

- go back to **Object Mode** > select your model again > go to **Material Properties** > you can delete the existing textures > and create a new **Material** by clicking **+**
- change **Base Colour** to texture > **Image Texture** > **Open** > select your UV mapped image 
- go to **Solid shading view** again to view your texture applied

 ## 4. Add video texture:

![Sample Video](3-tutorial-create-video-texture.mp4)

### **4.1** Add texture:
-  if you go back to  **Material Properties** you can add video (instead of image) as a **texture** > change **Frames** property to define how long (aka how many frames) the video should be playing > now go to **Render Properties** > select **Render Engine** > **Cycles**

- important note - it won’t play the video if you hit the play button in blender, that’s normal. You can just click through the timeline to make sure that your video will be playable in the render.

	
 ## 5. Move your Blender camera to an angle similar to how you'll have the beamer positioned in physical space and render your video: 

![Sample Video](4-tutorial-camera-alignment-and-render.mp4)

### **5.1** Specular and other material settings 
- go back to  **Material Properties** to adjust the material to you liking > we adjust **Specular** setting to make it flat and remove the shining. You might want to have a big contrast image to project, otherwise the natural texture of your object can overshadow it. 

### **5.2** import the camera that we prepared and that matches the beamer you will probably use:
- **file** > **import** > **Collada** > select the **projector_VivitekD952HD.dae** file > now in the **Scene Collection** you will see that the new camera appears - **Vivitek D952HD** > **select** this one and you can **delete** all other cameras in the scene, to make sure that you are rendering with the Vivitek one
- **select** the new camera > go to **Object Data Properties (little green camera icon)** > adjust **Viewport Display** settings so that it roughly fits your distance. This is only to help you aim and does not influence the rendering.

### **5.3** Render Settings:
- go to your render settings now - **Output Properties** (the icon looks like a little printer) > change **Frame Range** - End to match the length (in frames) of your rendered video > then make sure that the **Output** is set to a correct folder (this is where your video is going to be rendered to) and file name > change **File Format** to **ffmpeg video**

### **5.4** Time to render!
- click **Render** > **Render Animation** 
	
- **Important note**: if you're working with different layers in Blender, and are hiding some of them, make sure that you **unselect the little camera icon** next to the visibility icon. This means that the layer is not only invisible in your Blender workspace, but also in the render. 
￼
![Image](Screenshot_2021-12-19_at_19.34.59.png)



 ## 6. Project your video on your object in physical space (like, for real)! Be patient - you might need some time to find the fitting position for both the beamer and the object. In principle there are methods to calculate all of this with high precision by measuring all distances, sizes and resolutions, but we would rather like to see quick and dirty solutions and that you're able to spend more time experimenting with the media rather than show off as mathematicians. 

here's our result, which we did very (too) quick and with a very bad beamer:

![Sample Video](projectprojectorresult.mp4)

 ## 7. **Last but not least, following this tutorial can be a helpful method or a starting point for you, however, as we already mentioned in the brief, if you find your own unique way to incorporate projection in your work, and it makes sense, we will only be happy!**
